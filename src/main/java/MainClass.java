import com.jayway.jsonpath.JsonPath;

import java.util.Collection;
import java.util.Map;

/**
 * Created by aefimov on 17/04/2017.
 */
public class MainClass {

	public static void main(String[] args) {
		String value = "{\"RequestHeader\":null,\"RequestBody\":{\"materialId\":null,\"materialIds\":[\"807331\",\"807351\"" +
				",\"807352\",\"807353\",\"807354\",\"807641\",\"807650\",\"807815\",\"807887\",\"807913\",\"807931\"," +
				"\"6004574\"],\"materialIdType\":\"MaterialNumber\",\"addMaterialGroup\":true,\"addComment\":false,\"" +
				"addLargeText\":true,\"addMaterialParam\":true,\"addMaterialProperty\":true,\"addLinkedMaterial\"" +
				":false,\"linkedMaterialTypes\":[],\"addUnit\":true,\"addMediaContent\":true}}";
		String expected = "$.RequestBody.materialIds[?(@ == '807331')]";
		System.out.println(get(value, expected));
	}

	private static boolean get(String value, String expectedValue) {
		try {
			Object obj = JsonPath.read(value, expectedValue);
			if (obj instanceof Collection) {
				System.out.println(obj);
				Collection collection = (Collection) obj;
				if (collection.size() == 1) {
					System.out.println(collection.stream().findFirst().get());
				}
				return !collection.isEmpty();
			}

			if (obj instanceof Map) {
				System.out.println(obj);
				return !((Map) obj).isEmpty();
			}
			System.out.println(obj);
			return obj != null;
		} catch (Exception e) {
			String error;
			if (e.getMessage().equalsIgnoreCase("invalid path")) {
				error = "the JSON path didn't match the document structure";
			} else if (e.getMessage().equalsIgnoreCase("invalid container object")) {
				error = "the JSON document couldn't be parsed";
			} else {
				error = "of error '" + e.getMessage() + "'";
			}

			String message = String.format(
					"Warning: JSON path expression '%s' failed to match document '%s' because %s",
					expectedValue, value, error);
			System.out.println(message);
			return false;
		}
	}
}
